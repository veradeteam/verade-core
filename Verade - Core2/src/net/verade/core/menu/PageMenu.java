package net.verade.core.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class PageMenu extends Menu {

	HashMap<Integer, Inventory> pages = new HashMap<>();
	ArrayList<UUID> switchingPages = new ArrayList<UUID>();
	
	public PageMenu(Inventory inv) {
		super(inv);
		pages.put(1, inv);
	}
	
	@Override
	public void show(Player p) {
		show(p, 1);
	}
	
	public void show(Player p, int pageNum) {
		if (!pages.containsKey(pageNum)) {
			setPage(pageNum, createPage(pageNum));
		}
		if (viewers.contains(p))switchingPages.add(p.getUniqueId());
		showedPage(p, pageNum);
		show(p, pages.get(pageNum));
	}
	
	public void setItem(int slot, ItemStack item) {
		for (Inventory inv : pages.values()) {
			if (inv.getSize() > slot) {
				inv.setItem(slot, item);
			}else{
				slot-=inv.getSize();
			}
		}
	}
	
	public ItemStack getItem(int slot) {
		for (Inventory inv : pages.values()) {
			if (inv.getSize() > slot) {
				return inv.getItem(slot);
			}else{
				slot-=inv.getSize();
			}
		}
		return null;
	}
	
	public Inventory getPage(int page) {
		return pages.get(page);
	}
	
	public void setPage(int page, Inventory inv) {
		if (pages.containsKey(page)) {
			Inventory oldPage = pages.get(page);
			for (HumanEntity ent : oldPage.getViewers()) {
				show((Player) ent, inv);
			}
		}
		pages.put(page, inv);
	}
	
	public void removePage(int page) {
		if (page==1)return;
		if (pages.containsKey(page)) {
			Inventory oldPage = pages.get(page);
			for (HumanEntity ent : oldPage.getViewers()) {
				show((Player) ent, 1);
			}
		}
		pages.remove(page);
	}
	
	public abstract void showedPage(Player p, int page);
	public abstract Inventory createPage(int num);
	
	@Override
	public void onSwitchedMenu(Player p) {
		if (switchingPages.contains(p.getUniqueId()))return;
		viewers.remove(p);
	}
	
}

