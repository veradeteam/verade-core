package net.verade.core.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.scheduler.BukkitRunnable;

import net.verade.core.Verade;

public class MenuManager implements Listener {

	static MenuManager mm = new MenuManager();
	
	private MenuManager(){};
	
	public static MenuManager getManager() {
		return mm;
	}
	
	HashMap<UUID, Menu> menus = new HashMap<>();
	ArrayList<UUID> switchingMenus = new ArrayList<>();
	
	Verade plugin;
	
	public void setup(Verade plugin) {
		this.plugin=plugin;
		
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	public boolean isInMenu(Player p) {
		return menus.containsKey(p.getUniqueId());
	}
	
	public Menu getMenu(Player p) {
		return menus.get(p.getUniqueId());
	}
	
	public void openText(Player p, String text) {
		if (menus.containsKey(p.getUniqueId())) {
			Menu menu = menus.get(p.getUniqueId());
			if (menu instanceof TextMenu) {
				TextMenu textMenu = (TextMenu) menu;
				setSwitchMenus(p, true);
				textMenu.openText(p, text);
				setSwitchMenus(p, false);
			}
		}
	}
	
	public void setSwitchMenus(Player p, boolean switching) {
		if (switching) {
			switchingMenus.add(p.getUniqueId());
		}else{
			switchingMenus.remove(p.getUniqueId());
		}
	}
	
	public void setMenu(Player p, Menu menu) {
		menus.put(p.getUniqueId(), menu);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (menus.containsKey(e.getWhoClicked().getUniqueId())) {
			if (cooldown.contains(e.getWhoClicked().getUniqueId())) {
				e.setCancelled(true);
				return;
			}
			
			
			Menu menu = menus.get(e.getWhoClicked().getUniqueId());
			if (e.getClickedInventory() != null && e.getClickedInventory().equals(e.getView().getTopInventory())) {
				if (menu instanceof TextMenu && e.getClickedInventory() != null && e.getClickedInventory().getType().equals(InventoryType.ANVIL)) {
					e.setCancelled(menu.click(e));
					TextMenu textMenu = (TextMenu) menu;
					textMenu.clickTextMenu((Player) e.getWhoClicked(), e.getSlot());
				}else{
					e.setCancelled(menu.click(e));
					menu.click((Player) e.getWhoClicked(), e.getCurrentItem(), e.getSlot(), e.getClick());
				}	
			}else{
				e.setCancelled(menu.click(e));
			}
		}
	}
	
	@EventHandler
	public void onDrag(InventoryDragEvent e) {
		if (e.getWhoClicked() instanceof Player) {
			Player p = (Player) e.getWhoClicked();
			if (menus.containsKey(p.getUniqueId())) {
				if (e.getInventorySlots().size()==1) {
					e.setCancelled(true);
				}else{
					e.setCancelled(true);	
				}
			}
		}
	}
	
	HashSet<UUID> cooldown = new HashSet<>();
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		if (e.getPlayer() instanceof Player) {
			final Player p = (Player) e.getPlayer();
			if (!menus.containsKey(p.getUniqueId()))return;
			cooldown.add(p.getUniqueId());
			new BukkitRunnable() {

				@Override
				public void run() {
					cooldown.remove(p.getUniqueId());
				}
				
			}.runTaskLater(plugin, 10);
			
			Menu menu = menus.get(p.getUniqueId());	
			if (switchingMenus.contains(e.getPlayer().getUniqueId()) && menus.containsKey(p.getUniqueId())) {
				menu.onSwitchedMenu(p);
			}else{
				if (menu.onClose(p)) {
					menus.remove(p.getUniqueId());	
					p.updateInventory();
					if (p.getOpenInventory() != null)p.closeInventory();
				}
			}
		}
	}
	
}

