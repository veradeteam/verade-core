package net.verade.core.menu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.ContainerAnvil;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow;

public class SearchGUI {

    private class AnvilContainer extends ContainerAnvil {
        public AnvilContainer(EntityHuman entity) {
            super(entity.inventory, entity.world,new BlockPosition(0, 0, 0), entity);
        }

        @Override
        public boolean a(EntityHuman entityhuman) {
            return true;
        }
    }

    private TextMenu menu;
    @Getter private Inventory inv;

    public SearchGUI(TextMenu menu) {
        this.menu = menu;
    }

    public void open(Player player, String text) {
        EntityPlayer p = ((CraftPlayer) player).getHandle();

        AnvilContainer container = new AnvilContainer(p);

        //Set the items to the items from the inventory given
        inv = container.getBukkitView().getTopInventory();

        inv.setItem(0, generateItem(Material.NAME_TAG, text));

        //Counter stuff that the game uses to keep track of inventories
        int c = p.nextContainerCounter();

        //Send the packet
        p.playerConnection.sendPacket(new PacketPlayOutOpenWindow(c, "minecraft:anvil", new ChatMessage("Repairing"), 0));
        //Set their active container to the container
        p.activeContainer = container;

        //Set their active container window id to that counter stuff
        p.activeContainer.windowId = c;

        //Add the slot listener
        p.activeContainer.addSlotListener(p);
    }

	public void click(Player p, int slot) {
		if (slot==2) {
			String text;
			if (inv.getItem(2) != null) {
				text = ChatColor.stripColor(inv.getItem(2).getItemMeta().getDisplayName());	
			}else{
				text = ChatColor.stripColor(inv.getItem(0).getItemMeta().getDisplayName());
			}
			inv.setItem(0, null);
			inv.setItem(1, null);
			inv.setItem(2, null);
			menu.inputText(p, text);
		}
	}
    
    public ItemStack generateItem(Material type, String name) {
    	ItemStack item = new ItemStack(type);
    	ItemMeta meta = item.getItemMeta();
    	meta.setDisplayName(name);
    	item.setItemMeta(meta);
		return item;
    }
}
    
