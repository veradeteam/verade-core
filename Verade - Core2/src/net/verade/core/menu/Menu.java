package net.verade.core.menu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

@Getter
public abstract class Menu {

	List<Player> viewers = new ArrayList<>();
	public Inventory inv;
	
	public Menu(Inventory inv) {
		this.inv=inv;
	}
	
	public boolean onClose(Player p) {
		viewers.remove(p);
		return true;
	}
	
	public void onSwitchedMenu(Player p) {
		viewers.remove(p);
	}
	
	public void show(Player p) {
		show(p, inv);
	}
	
	protected void show(Player p, Inventory inv) {
		MenuManager.getManager().setSwitchMenus(p, true);
		
		if (!viewers.contains(p))viewers.add(p);
		p.openInventory(inv);
		MenuManager.getManager().setMenu(p, this);
		
		MenuManager.getManager().setSwitchMenus(p, false);
	}

	public abstract void click(Player p, ItemStack item, int slot, ClickType click);
	
	public boolean click(InventoryClickEvent e) {
		return true;
	}
	
	public ItemStack generateItem(Material type, byte data, int amount, String name, String... lines) {
		ItemStack item = new ItemStack(type, amount, data);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		List<String> lore = new ArrayList<>();
		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public ItemStack generateItem(Material type, byte data, String name, String... lines) {
		ItemStack item = new ItemStack(type, 1, data);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		List<String> lore = new ArrayList<>();
		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public ItemStack generateItem(Material type, int amount, String name, String... lines) {
		ItemStack item = new ItemStack(type, amount);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		List<String> lore = new ArrayList<>();
		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public ItemStack generateItem(Material type, String name, String... lines) {
		ItemStack item = new ItemStack(type, 1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		List<String> lore = new ArrayList<>();
		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public ItemStack generateSkull(String playerName, String displayName, String... lines) {
		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte)3);
		SkullMeta meta = (SkullMeta) skull.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		List<String> lore = new ArrayList<>();
		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		meta.setOwner(playerName);
		skull.setItemMeta(meta);
		return skull;
	}
	
	public ItemStack addGlow(ItemStack item){
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
	
	public ItemStack removeGlow(ItemStack item){
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        tag.remove("ench");
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
	
	public String getFriendlyName(ItemStack item) {
		if (item.hasItemMeta()) {
			ItemMeta meta = item.getItemMeta();
			if (meta.hasDisplayName()) {
				return ChatColor.stripColor(meta.getDisplayName());
			}
		}
		return "";
	}
	
	public void setName(ItemStack item, String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
	}
	
	public void changeLore(ItemStack item, int line, String newText) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore = meta.getLore();
		lore.set(line, ChatColor.translateAlternateColorCodes('&', newText));
		meta.setLore(lore);
		item.setItemMeta(meta);
	}
	
	public void addToLore(ItemStack item, String... newLines) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore;
		if (meta.hasLore()) {
			lore = meta.getLore();	
		}else{
			lore = new ArrayList<>();
		}
		for (String line : newLines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
	}
	
	public void setLore(ItemStack item, String... lines) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore = new ArrayList<>();
		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
	}
	
	public void insertLoreLines(ItemStack item, int afterLines, String... lines) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore = new ArrayList<>();
		
		List<String> oldLore = meta.getLore();
		
		int currentLine = 0;
		while (currentLine < afterLines) {
			lore.add(oldLore.get(currentLine));
			currentLine++;
		}

		for (String line : lines) {
			lore.add(ChatColor.translateAlternateColorCodes('&', line));
		}
		
		while (currentLine < oldLore.size()) {
			lore.add(oldLore.get(currentLine));
			currentLine++;
		}
		
		meta.setLore(lore);
		item.setItemMeta(meta);
	}
	
	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
	
	public static ItemStack unbreakable(ItemStack item) {
		net.minecraft.server.v1_8_R3.ItemStack stack = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = (stack.hasTag()) ? stack.getTag() : new NBTTagCompound();
		tag.setInt("Unbreakable", 1);
		stack.setTag(tag);
		return CraftItemStack.asBukkitCopy(stack);
	}
	
	public static ItemStack hideFlags(ItemStack item) {
		net.minecraft.server.v1_8_R3.ItemStack stack = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = (stack.hasTag()) ? stack.getTag() : new NBTTagCompound();
		tag.setInt("HideFlags", 63);
		stack.setTag(tag);
		return CraftItemStack.asBukkitCopy(stack);
	}
	
}
