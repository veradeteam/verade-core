package net.verade.core.menu;

import org.bukkit.entity.Player;

public interface TextMenu {

	public abstract void inputText(Player p, String text);

	public abstract void openText(Player p, String text);

	public abstract void clickTextMenu(Player p, int slot);
	
}