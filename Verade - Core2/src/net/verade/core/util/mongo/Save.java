package net.verade.core.util.mongo;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bson.Document;

import com.mongodb.BasicDBObject;

import lombok.Setter;

public class Save {

	@Setter
	public BasicDBObject cause;
	
	public HashMap<String, Object> sets = new HashMap<>();
	public HashMap<String, Object> unsets = new HashMap<>();
	public HashMap<String, Integer> incs = new HashMap<>();
	public HashMap<String, Object> pulls = new HashMap<>();
	public HashMap<String, Object> pushes = new HashMap<>();
	public HashMap<String, Object> addToSets = new HashMap<>();
	
	public Save set(String key, Object value) { sets.put(key, value);return this;}
	public Save unset(String key, Object value) { unsets.put(key, value);return this;}
	public Save inc(String key, int amount) { incs.put(key, amount); return this;}
	public Save pull(String key, Object value) { sets.put(key, value);return this;}
	public Save push(String key, Object value) { pushes.put(key, value);return this;}
	public Save addToSet(String key, Object value) { addToSets.put(key, value);return this;}
	
	public Save(){}
	public Save(BasicDBObject cause) {
		this.cause=cause;
	}
	
	public Document build() {
		Document doc = new Document();
		if (!sets.isEmpty()) {
			Document sets = new Document();
			for (Entry<String, Object> set : this.sets.entrySet()) {
				sets.append(set.getKey(), set.getValue());
			}
			doc.append("$set", sets);
		}
		if (!unsets.isEmpty()) {
			Document unsets = new Document();
			for (Entry<String, Object> unset : this.unsets.entrySet()) {
				unsets.append(unset.getKey(), unset.getValue());
			}
			doc.append("$unset", unsets);
		}
		if (!incs.isEmpty()) {
			Document incs = new Document();
			for (Entry<String, Integer> inc : this.incs.entrySet()) {
				incs.append(inc.getKey(), inc.getValue());
			}
			doc.append("$inc", incs);
		}
		if (!pulls.isEmpty()) {
			Document pulls = new Document();
			for (Entry<String, Object> pull : this.pulls.entrySet()) {
				pulls.append(pull.getKey(), pull.getValue());
			}
			doc.append("$pull", pulls);
		}
		if (!addToSets.isEmpty()) {
			Document addToSets = new Document();
			for (Entry<String, Object> addToSet : this.addToSets.entrySet()) {
				addToSets.append(addToSet.getKey(), addToSet.getValue());
			}
			doc.append("$addToSet", addToSets);
		}
		if (!pushes.isEmpty()) {
			Document pushes = new Document();
			for (Entry<String, Object> push : this.pushes.entrySet()) {
				pushes.append(push.getKey(), push.getValue());
			}
			doc.append("$push", pushes);
		}
		return doc;
	}
	
}

