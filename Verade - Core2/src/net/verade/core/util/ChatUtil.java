package net.verade.core.util;

import org.bukkit.ChatColor;

public class ChatUtil {
	
	public static String colorize(String s){
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
