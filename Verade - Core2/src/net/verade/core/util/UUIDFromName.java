package net.verade.core.util;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import net.verade.core.MongoManager;


public class UUIDFromName {
	
	String uuid = null;
	
	public UUIDFromName(String name) {
		MongoCursor<Document> iterator = MongoManager.getManager().getDatabase().getCollection("players").find(new Document("username", name)).iterator();
		if (iterator.hasNext()) {
			uuid = iterator.next().getString("uuid");
		}
		iterator.close();
	}
	
	public String getUUID() {
		return uuid;
	}
	
}
