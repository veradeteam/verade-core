package net.verade.core.util;

import lombok.Getter;
import lombok.Setter;

public class CoreSettings {

	@Getter @Setter public static String serverName = "{FETCHING}";
	@Getter @Setter public static boolean controlTabList = true;
	@Getter @Setter public static boolean controlDisplayName = true;
	@Getter @Setter public static boolean allowColoredName = true;
	@Getter @Setter public static boolean eulaCompliant = false;
	
}

