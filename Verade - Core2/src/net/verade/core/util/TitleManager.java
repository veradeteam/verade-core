package net.verade.core.util;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;

public class TitleManager {

	public static void broadcastTitle(String title, String subtitle,
			int fadeIn, int stay, int fadeOut) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			displayTitle(p, title, subtitle, fadeIn, stay, fadeOut);
		}
	}

	public static void broadcastActionbar(String message, int fadeIn, int stay, int fadeOut) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			displayActionbar(p, message, fadeIn, stay, fadeOut);
		}
	}

	public static void displayTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		sendTimes(p, fadeIn, stay, fadeOut);
		sendSubtitle(p, subtitle);
		sendTitle(p, title);
	}

	public static void displayActionbar(Player p, String message, int fadeIn,
		int stay, int fadeOut) {
		sendTimes(p, fadeIn, stay, fadeOut);
		sendActionbar(p, message); 
	}

	public static void sendTimes(Player p, int fadeIn, int stay, int fadeOut) {
		PacketPlayOutTitle packet = new PacketPlayOutTitle(fadeIn, stay,
				fadeOut);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}

	public static void sendSubtitle(Player p, String subtitle) {
		IChatBaseComponent subtitleComponent = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle.replaceAll("&", "§") + "\"}");
		PacketPlayOutTitle subtitlePacket = new PacketPlayOutTitle(
				PacketPlayOutTitle.EnumTitleAction.SUBTITLE, subtitleComponent);
		((CraftPlayer) p).getHandle().playerConnection
				.sendPacket(subtitlePacket);
	}

	public static void sendTitle(Player p, String title) {
		IChatBaseComponent titleComponent = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title.replaceAll("&", "§") + "\"}");
		PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(
				PacketPlayOutTitle.EnumTitleAction.TITLE, titleComponent);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(titlePacket);
	}

	public static void sendActionbar(Player player, String message) {
	      IChatBaseComponent bar = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message.replaceAll("&", "§") + "\"}");
	      EntityPlayer p = ((CraftPlayer)player).getHandle();
	      PacketPlayOutChat packet = new PacketPlayOutChat(bar, (byte)2);
	      p.playerConnection.sendPacket(packet);
	  }

}

