package net.verade.core.cmds;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;

public abstract class VeradeCommand extends BukkitCommand {
	
	
    private String label;
    private String desc;
    private String permission = "";
    private List<String> aliases;

    public VeradeCommand(String label, String desc, String permission, String... aliases) {
            super(label);
            this.label = label;
            this.desc = desc;
            this.permission = permission;
            this.aliases = Arrays.asList(aliases);
    }
    
    @Override
    public String getLabel() { 
            return this.label;
    }

    @Override
    public String getDescription() {
            return desc;
    }

    @Override
    public List<String> getAliases() {
        return aliases;
    }

    public String getPermission() {
            return permission;
    }

    public abstract void execute(CommandSender sender, String[] args);
   
	public void noPermission(CommandSender sender, String[] args) {
		sender.sendMessage(colorize("&8[&c&l*&8]&7 You do not have permission to execute this command."));
	}

    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
            if (!permission.isEmpty() && !sender.hasPermission(permission)) {
            		noPermission(sender, args);
                    return true;
            }
            execute(sender, args);
            return true;
    }

    public void registerMe() {
    	((CraftServer) Bukkit.getServer()).getCommandMap().register("apollocommand", this);
    }
	
    public String colorize(String text) {
    	return ChatColor.translateAlternateColorCodes('&', text);
    }
    
	public Player getPlayer(String user) {
		Player target = null;
		if (user.length()==36) {
			target = Bukkit.getPlayer(UUID.fromString(user));
		}else{
			target = Bukkit.getPlayer(user);
		}
		return target;
	}
	
}
