package net.verade.core.cmds;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import lombok.Getter;

@Getter
public abstract class VeradeSubCommand {

	
	 private String label;
	 private String desc;
	 private String[] aliases;
	 
	 protected VeradeSubCommand(String label, String desc, String... aliases) {
		 this.label = label;
		 this.desc = desc;
		 this.aliases = aliases;
	 }

	 public abstract void execute(CommandSender sender, String[] args);
	   
	 public void noPermission(CommandSender sender, String[] args) {
		 sender.sendMessage(colorize("&8[&c&l*&8]&7 You do not have permission to execute this command."));
	 }
		
	 public String colorize(String text) {
		 return ChatColor.translateAlternateColorCodes('&', text);
	 }
	
	 public Player getPlayer(String user) {
		 Player target = null;
		 if (user.length()==36) {
			 target = Bukkit.getPlayer(UUID.fromString(user));
		 }else{
			 target = Bukkit.getPlayer(user);
		 }	
		 return target;
	 }
	 
}
