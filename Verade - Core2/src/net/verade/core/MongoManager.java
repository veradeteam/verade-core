package net.verade.core;

import java.util.Arrays;
import java.util.HashMap;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

public class MongoManager {

	Verade verade;
	MongoClient mongoClient = null;
	MongoDatabase database = null;
	
	private static MongoManager manager = new MongoManager();
	private MongoManager(){}
	
	public static MongoManager getManager() {
		return manager;
	}
	
	public void setup(Verade verade) {
		this.verade=verade;
		
		String host = Verade.getPlugin().getConfig().getString("database.host");
		int port = Verade.getPlugin().getConfig().getInt("database.port");
		String username = Verade.getPlugin().getConfig().getString("database.user");
		String database = Verade.getPlugin().getConfig().getString("database.database");
		String password = Verade.getPlugin().getConfig().getString("database.password");
		connect(host, port, username, database, password);
	}
	
	public void connect(String host, int port, String username, String database, String password) {
		MongoCredential credential = MongoCredential.createCredential(username, database, password.toCharArray());
		this.mongoClient = new MongoClient(new ServerAddress(host, port), Arrays.asList(credential));
		this.database = mongoClient.getDatabase(database);
		if (database != null) {
			Bukkit.getLogger().info("[Verade-Core] Connected to Mongo!");	
		}
	}
	
	public MongoDatabase getDatabase() {
		return database;
	}
	
	public void set(final BasicDBObject cause, final HashMap<String, Object> sets) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$set", sets));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void inc(final BasicDBObject cause, final HashMap<String, Integer> inc) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$inc", inc));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void inc(final BasicDBObject cause, final Document inc) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$inc", inc));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void unset(final BasicDBObject cause, final HashMap<String, Object> unsets) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$unset", unsets));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void addToSet(final BasicDBObject cause, final String path, final Object value, boolean addIfAlreadyExists) {
		if (addIfAlreadyExists) {
			new BukkitRunnable() {	
				@Override
				public void run() {
					MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$push", new Document(path, value)));
				}
			}.runTaskAsynchronously(verade);	
		}else{
			new BukkitRunnable() {	
				@Override
				public void run() {
					MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$addToSet", new Document(path, value)));
				}
			}.runTaskAsynchronously(verade);
		}
	}
	
	public void removeFromSet(final BasicDBObject cause, final String path, final Object value) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, new BasicDBObject("$pull", new Document(path, value)));
			}
		}.runTaskAsynchronously(verade);
	}
	
}


