package net.verade.core.npcs;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.trait.TraitName;
import net.citizensnpcs.api.util.DataKey;
import net.verade.core.Verade;

@TraitName("DespawnOnStart")
public class DespawnOnRefreshTrait extends Trait {
	
	public DespawnOnRefreshTrait() {
		super("DespawnOnStart");
	}
	
    @Override
    public void load(DataKey key) {
        Bukkit.getLogger().info("Loading Despawn Trait!");
        new BukkitRunnable() {
            int time = 10;
            public void run() {
                time--;
                Bukkit.getLogger().info("Removing NPC in " + time + " seconds.");
                if (time == 0) {
                    Bukkit.getLogger().info("Removed OLD NPC");
                    CitizensAPI.getNPCRegistry().deregister(npc);
                    npc.despawn();
                    npc.destroy();
                    cancel();
                    return;
                }
            }
        }.runTaskTimer(Verade.getPlugin(), 0, 20);
    }

}
