package net.verade.core.scoreboard;

import java.util.Arrays;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardUtil {
	
	public static String[] cut(String[] content) {
		String[] elements = Arrays.copyOf(content, 16);

		if (elements[0] == null)
			elements[0] = "Main Scoreboard";

		if (elements[0].length() > 32)
			elements[0] = elements[0].substring(0, 32);

		for (int i = 1; i < elements.length; i++)
			if (elements[i] != null)
				if (elements[i].length() > 40)
					elements[i] = elements[i].substring(0, 40);
		

		return elements;
	}

	public static boolean SidebarDisplay(Player p, String... elements) {
		elements = cut(elements);

		try {
			if (p.getScoreboard() == null || p.getScoreboard() == Bukkit.getScoreboardManager().getMainScoreboard()
					|| p.getScoreboard().getObjectives().size() != 1) {
				p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			}

			if (p.getScoreboard().getObjective(p.getUniqueId().toString().substring(0, 16)) == null) {
				p.getScoreboard().registerNewObjective(p.getUniqueId().toString().substring(0, 16), "dummy");
				p.getScoreboard().getObjective(p.getUniqueId().toString().substring(0, 16))
						.setDisplaySlot(DisplaySlot.SIDEBAR);
			}

			p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(elements[0]);

			for (int i = 1; i < elements.length; i++)
				if (elements[i] != null)
					if (p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore(elements[i]).getScore() != 16
							- i) {
						p.getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore(elements[i]).setScore(16 - i);
						for (String string : p.getScoreboard().getEntries())
							if (p.getScoreboard().getObjective(p.getUniqueId().toString().substring(0, 16))
									.getScore(string).getScore() == 16 - i)
								if (!string.equals(elements[i]))
									p.getScoreboard().resetScores(string);

					}

			for (String entry : p.getScoreboard().getEntries()) {
				boolean toErase = true;
				for (String element : elements) {
					if (element != null && element.equals(entry)
							&& p.getScoreboard().getObjective(p.getUniqueId().toString().substring(0, 16))
									.getScore(entry).getScore() == 16 - Arrays.asList(elements).indexOf(element)) {
						toErase = false;
						break;
					}
				}

				if (toErase)
					p.getScoreboard().resetScores(entry);

			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean SidebarDisplay(Collection<Player> players, String[] elements) {
		for (Player player : players)
			if (!SidebarDisplay(player, elements))
				return false;

		return true;
	}

	public static boolean SidebarDisplay(Collection<Player> players, Scoreboard board, String... elements) {
		try {
			String objName = "VERADETEST";

			if (board == null)
				board = Bukkit.getScoreboardManager().getNewScoreboard();

			elements = cut(elements);

			for (Player player : players)
				if (player.getScoreboard() != board)
					player.setScoreboard(board);

			if (board.getObjective(objName) == null) {
				board.registerNewObjective(objName, "dummy");
				board.getObjective(objName).setDisplaySlot(DisplaySlot.SIDEBAR);
			}

			board.getObjective(DisplaySlot.SIDEBAR).setDisplayName(elements[0]);

			for (int i = 1; i < elements.length; i++)
				if (elements[i] != null
						&& board.getObjective(DisplaySlot.SIDEBAR).getScore(elements[i]).getScore() != 16 - i) {
					board.getObjective(DisplaySlot.SIDEBAR).getScore(elements[i]).setScore(16 - i);
					for (String string : board.getEntries())
						if (board.getObjective(objName).getScore(string).getScore() == 16 - i)
							if (!string.equals(elements[i]))
								board.resetScores(string);

				}

			for (String entry : board.getEntries()) {
				boolean toErase = true;
				for (String element : elements) {
					if (element != null && element.equals(entry) && board.getObjective(objName).getScore(entry).getScore() 
							== 16 - Arrays.asList(elements).indexOf(element)) {
						toErase = false;
						break;
					}
				}

				if (toErase)
					board.resetScores(entry);

			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
