package net.verade.core.chat;
  
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;


public class ChatListener implements Listener {
	
	 
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onChat(PlayerChatEvent event){
		Player player = event.getPlayer();
		
		if(player.hasPermission("rank.bandit")){
			event.setFormat(ChatColor.DARK_RED +""+ ChatColor.BOLD + "Bandit " + ChatColor.GRAY + player.getName() + "» " + ChatColor.WHITE + event.getMessage());
		}
		if(player.hasPermission("rank.royal")){
			event.setFormat(ChatColor.DARK_PURPLE +""+ ChatColor.BOLD + "Royal " + ChatColor.GRAY + player.getName() + "» " + ChatColor.WHITE + event.getMessage());
		}
		if(player.hasPermission("rank.mod")){
			event.setFormat(ChatColor.AQUA +""+ ChatColor.BOLD + "Mod " + ChatColor.GRAY + player.getName() + "» " + ChatColor.WHITE + event.getMessage());
		}
		if(player.hasPermission("rank.admin")){
			event.setFormat(ChatColor.RED +""+ ChatColor.BOLD + "Admin " + ChatColor.GRAY + player.getName() + "» " + ChatColor.WHITE + event.getMessage());
		}
		if(player.hasPermission("rank.owner")){
			event.setFormat(ChatColor.BLUE +""+ ChatColor.BOLD + "Owner " + ChatColor.GRAY + player.getName() + "» " + ChatColor.WHITE + event.getMessage());
		}
	}
	
	@EventHandler
	public void onChat(PlayerCommandPreprocessEvent event){
		Player player = event.getPlayer();
		
		if(event.getMessage().contains("help")){
			//TODO 
		}
	}
}
