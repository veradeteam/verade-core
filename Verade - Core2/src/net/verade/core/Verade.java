package net.verade.core;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitInfo;
import net.verade.core.chat.ChatListener;
import net.verade.core.menu.MenuManager;
import net.verade.core.npcs.DespawnOnRefreshTrait;
import net.verade.core.profiles.ProfileManager;
import net.verade.core.profiles.credits.CreditManager;
import net.verade.core.profiles.levels.LevelManager;
import net.verade.core.profiles.perms.PermissionManager;
import net.verade.core.system.ServerNameGetter;
import net.verade.core.system.TabListManager;

public class Verade extends JavaPlugin {
	
	@Getter TabListManager tabListManager;
	@Getter public static Verade plugin;
	
	@Override
	public void onEnable(){
		plugin = this;
		Bukkit.getLogger().info("[Verade-Core] Enabling Verade Core!");
		saveDefaultConfig();
		
		LevelManager.getManager().setup(this);
		MongoManager.getManager().setup(this);
		PermissionManager.getManager().setup(this);
		ProfileManager.getManager().setup(this);
		
		MenuManager.getManager().setup(this);
		CreditManager.getManager().setup(this);
		
		Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
		
		ServerNameGetter nameGetter = new ServerNameGetter(this);
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", nameGetter);
		nameGetter.findServerName();
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		CitizensAPI.getTraitFactory().registerTrait(TraitInfo.create(DespawnOnRefreshTrait.class).withName("DespawnOnStart"));
		tabListManager = new TabListManager(this);
		
	}

}
