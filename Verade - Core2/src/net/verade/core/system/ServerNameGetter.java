package net.verade.core.system;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import lombok.AllArgsConstructor;
import net.verade.core.Verade;
import net.verade.core.util.CoreSettings;

@AllArgsConstructor
public class ServerNameGetter implements PluginMessageListener {
	
	Verade plugin;
	
	public void findServerName() {
	    new BukkitRunnable() {
			@Override
			public void run() {
				if (CoreSettings.getServerName() == "{FETCHING}") {
					sendOutForServerName();
				}else{
					cancel();
				}
			}
		}.runTaskTimer(plugin, 0, 20);
	}
	
	private void sendOutForServerName() {
		 ByteArrayDataOutput out = ByteStreams.newDataOutput();
		 out.writeUTF("GetServer");
		 Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
		 if (player==null)return;
		 player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
	}
	
	@Override
	public void onPluginMessageReceived(String channel, Player p, byte[] message) {
	    if (!channel.equals("BungeeCord")) {
	        return;
	    }
	    ByteArrayDataInput in = ByteStreams.newDataInput(message);
	    String subchannel = in.readUTF();
	    if (subchannel.equals("GetServer")) {
	    	CoreSettings.setServerName(in.readUTF());
	    }
	}
	
}

