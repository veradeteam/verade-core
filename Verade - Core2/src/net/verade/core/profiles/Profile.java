package net.verade.core.profiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.bson.Document;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.mongodb.BasicDBObject;

import lombok.Getter;
import lombok.Setter;
import net.verade.core.MongoManager;
import net.verade.core.Verade;
import net.verade.core.profiles.perms.VeradePermission;
import net.verade.core.profiles.perms.PermissionManager;


@Getter
@Setter
public class Profile {

	HashMap<String, Object> options;
	boolean loaded = false;
	Verade verade;
	String UUID;
	String username;
	String IP;
	
	@Getter @Setter ChatColor nameColor;
	
	List<String> groups = new ArrayList<>(); 
	TreeMap<Integer, String> activeGroups = new TreeMap<>();
	List<VeradePermission> permissions = new ArrayList<>();
	Document info;

	
	@Getter int credits = 0;
	
	int gameplay = 0;
	long seen = 0;
	long join = 0;
	
	int level = 1;
	int exp = 0;
	
	@Getter int voteTokens = 0;	
	
	public void load() {
		loadPerms();
		loadCredits();
		loadGameplay();

		if (info.containsKey("color")) {
			nameColor = ChatColor.valueOf(info.getString("color"));
		}
	}
	
	public Profile(Verade verade, String UUID, Document info) {
		this.verade=verade;
		this.UUID=UUID;
		this.info=info;
		this.username=info.getString("username");
	}
	
	public void loadGameplay() {
		if (info.containsKey("gameplay")) {
			gameplay = info.getInteger("gameplay");
		}
		if (info.containsKey("join")) {
			join = info.getLong("join");
		}
		if (info.containsKey("seen")) {
			seen = info.getLong("seen");
		}
		if (info.containsKey("level")) {
			level = info.getInteger("level");
		}
		if (info.containsKey("exp")){
			exp = info.getInteger("exp");
		}
	}
	
	@Deprecated
	public String getLatestUsername() {
		return username;
	}
	
	//====================================={ Game Profiles }=====================================\\
	
	//====================================={Credits / Item Packs / Crates}=====================================\\
	
	public void loadCredits() {
		if (info.get("credits") instanceof Integer) {
			credits = info.getInteger("credits");
		}else{
			double curCredits = (double) info.get("credits");
			credits = (int) curCredits;
		}
	}
	
	public void modifyCredits(int amount) {
		credits+=amount;
	}
	
	//====================================={DATABASE}=====================================\\
	
	public void reloadFromDoc() {
		permissions.clear();
		groups.clear();
		credits = 0;
		load();
	}
	
	public void reloadDoc(boolean sync) {
		if (sync) {
			info = MongoManager.getManager().getDatabase().getCollection("players").find(new Document("uuid", UUID)).first();
		}else{
			new BukkitRunnable() {
				@Override
				public void run() {
					info = MongoManager.getManager().getDatabase().getCollection("players").find(new Document("uuid", UUID)).first();
				}
			}.runTaskAsynchronously(verade);	
		}
	}
	
	public void save(final HashMap<String, Object> unsets, final HashMap<String, Object> sets) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				boolean unsetsEmpty = unsets.isEmpty();
				boolean setsEmpy = sets.isEmpty();
				
				if (!unsetsEmpty && !setsEmpy) {
					MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$unset", unsets).append("$set", sets));	
				}else{
					if (unsetsEmpty) {
						if (setsEmpy)return;
						MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$set", sets));
					}else{
						MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$unset", unsets));
					}
				}
			}
		}.runTaskAsynchronously(verade);
	}

	public void set(final HashMap<String, Object> sets) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$set", sets));
			}
		}.runTaskAsynchronously(verade);
	}

	public void set(final String key, final Object value) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$set", new Document(key, value)));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void unset(final Map<String, Object> unsets) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$unset", unsets));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void addToSet(final String path, final Object value, final boolean addIfAlreadyExists) {
		if (addIfAlreadyExists) {
			new BukkitRunnable() {	
				@Override
				public void run() {
					MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$push", new Document(path, value)));
				}
			}.runTaskAsynchronously(verade);	
		}else{
			new BukkitRunnable() {	
				@Override
				public void run() {
					MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$addToSet", new Document(path, value)));
				}
			}.runTaskAsynchronously(verade);
		}
	}
	
	public void removeFromSet(final String path, final Object value) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$pull", new Document(path, value)));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void unset(final Document unset) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", UUID), new BasicDBObject("$unset", unset));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void inc(final Document inc) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", UUID), new BasicDBObject("$inc", inc));
			}
		}.runTaskAsynchronously(verade);
	}
	
	public void inc(final HashMap<String, Object> incs) {
		new BukkitRunnable() {	
			@Override
			public void run() {
				MongoManager.getManager().getDatabase().getCollection("players").updateOne(new BasicDBObject("uuid", getUUID()), new BasicDBObject("$inc", incs));
			}
		}.runTaskAsynchronously(verade);
	}
	
	//====================================={PERMS}=====================================\\
	
	@SuppressWarnings("unchecked")
	public void loadPerms() {
		if (info.containsKey("perms")) {
			Document perms = (Document) info.get("perms");
			if (perms.containsKey("groups")) {
				List<String> groups = (List<String>) perms.get("groups");
				for (String group : groups) {
					this.groups.add(group);
					if (PermissionManager.getManager().isGroup(group)) {
						int weight = PermissionManager.getManager().getPermissionGroup(group).getWeight();
						if (weight != -1) {
							for (String inherit : PermissionManager.getManager().getPermissionGroup(group).getInherits()) {
								if (PermissionManager.getManager().getPermissionGroup(inherit).getWeight() > weight) {
									continue;
								}else{
									if (this.activeGroups.containsValue(inherit)) {
										this.activeGroups.values().removeAll(Collections.singleton(group));
									}	
								}
							}
							this.activeGroups.put(weight, group);
						}	
					}
				}
			}
			if (perms.containsKey("perms")) {
				List<String> playerPerms = (List<String>) perms.get("perms");
				for (String perm : playerPerms) {
					permissions.add(new VeradePermission(perm));
				}
			}
		}
		if (activeGroups.isEmpty())activeGroups.put(PermissionManager.getManager().getPermissionGroup("Default").getWeight(), "Default");
	}
	
	public void addPermission(String permString) {
		VeradePermission permission;
		if (permString.startsWith("-")) {
			permission = new VeradePermission(permString.substring(1), false);
		}else{
			permission = new VeradePermission(permString, true);
		}
		boolean hasPerm = false;
		
		for (VeradePermission playerPerm : permissions) {
			if (playerPerm.equals(permission)) {
				hasPerm=true;
				break;
			}
		}
		
		if (!hasPerm) {
			permissions.add(permission);		
			addToSet("perms.perms", permString, false);	
		}
	}
	
	public void removePermission(String perm) {
		VeradePermission permission;
		if (perm.startsWith("-")) {
			permission = new VeradePermission(perm.substring(1), false);
		}else{
			permission = new VeradePermission(perm, true);
		}
		boolean hadPerm = false;
		for (VeradePermission playerPerm : permissions) {
			if (playerPerm.equals(permission)) {
				hadPerm=true;
				permissions.remove(playerPerm);
				break;
			}
		}
		
		if (hadPerm) {
			if (permissions.size()==0) {
				if (groups.size()==0) {
					unset(new Document("perms", ""));
				}else{
					unset(new Document("perms.perms", ""));	
				}
			}else{
				removeFromSet("perms.perms", perm);		
			}
		}
	}
	
	public void addGroup(String group) {
		if (groups.contains(group)) {
			return;
		}
		groups.add(group);
		
		if (PermissionManager.getManager().isGroup(group)) {
			activeGroups.put(PermissionManager.getManager().getPermissionGroup(group).getWeight(), group);
		}
		addToSet("perms.groups", group, false);		
	}
	
	public void removeGroup(String group) {
		if (groups.contains(group)) {
			groups.remove(group);
			if (PermissionManager.getManager().isGroup(group)) {
				activeGroups.values().removeAll(Collections.singleton(group));
			}
			if (groups.size()==0) {
				if (permissions.size()==0) {
					unset(new Document("perms", ""));
				}else{
					unset(new Document("perms.groups", ""));	
				}
			}else{
				removeFromSet("perms.groups", group);		
			}
		}
	}

	public boolean isGroup(String group) {
		return groups.contains(group);
	}
	
	public boolean isActiveGroup(String group) {
		return activeGroups.containsValue(group);
	}

	@Deprecated
	public String getHighestWeightRank() {
		return activeGroups.values().toArray(new String[]{})[activeGroups.values().size()-1];
	}
	
	public String getHighestWeightLegacyRank() {
		String group = "Default";
		for (String pot : activeGroups.values()) {
			if (pot.startsWith("highestdonorrank"))group=pot;
		}
		return group;
	}
	
	public String getHighestWeightNonLegacyRank() {
		String group = "Default";
		for (String pot : activeGroups.values()) {
			if (!pot.startsWith("highestdonorrank"))group=pot;
		}
		return group;
	}
	
	public boolean isOptionSet(String key) {
		return options.containsKey(key);
	}
	
	public Object getOption(String key) {
		return options.get(key);
	}
}

