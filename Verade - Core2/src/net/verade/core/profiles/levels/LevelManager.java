package net.verade.core.profiles.levels;

import net.verade.core.Verade;

public class LevelManager {
	
	static LevelManager lm = new LevelManager();
	static LevelHandler levelHandler = new LevelHandler();
	
	public static LevelManager getManager(){
		return lm;
	}
	
	public static LevelHandler getHandler(){
		return levelHandler; 
	}
	
	Verade plugin;
	
	public void setup(Verade plugin){
		this.plugin=plugin;
		levelHandler.initializeLevels();
	}
}
