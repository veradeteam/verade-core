package net.verade.core.profiles.levels;

import org.bukkit.entity.Player;

import java.util.*;

public class LevelHandler {

	private int intitalexp;
	private int starterlevel;

	private int baseCurve;
	private int difficulty;

	private int maximumlevelCapacity;

	private Map<UUID, Integer> users = new HashMap<>();
	private ArrayList<Integer> levelcurve = new ArrayList<>();

	public LevelHandler() {
		intitalexp = 0;
		starterlevel = 1;
		baseCurve = 30;
		difficulty = 5;
		maximumlevelCapacity = 101;
	}
	  
	@SuppressWarnings("null")
	public Integer getExp(Player player) {
		UUID uuid = player.getUniqueId();
		Integer value = users.get(uuid);
		int expAmount = 0;
 
		if (value != null) {
			expAmount = value;
		} else {
			if (users.containsKey(uuid) && users.containsValue(uuid)) {
				expAmount = value;
			} else {
				users.put(uuid, intitalexp);
				expAmount = intitalexp;
				return expAmount;
			}
		}

		return expAmount;

	}

	public void addExp(Player player, int amount) {

		UUID uuid = player.getUniqueId();

		users.computeIfPresent(uuid, (num, val) -> val + amount);

		users.computeIfAbsent(uuid, num -> intitalexp);

	}

	public void setExp(Player player, int amount) {

		UUID uuid = player.getUniqueId();

		users.computeIfPresent(uuid, (num, val) -> amount);

		users.computeIfAbsent(uuid, num -> intitalexp);
	}
	
	public void subtractExp(Player player, int amount) {
		UUID uuid = player.getUniqueId();
		Integer value = users.get(uuid);

		if (value != null) {
			if (users.get(uuid) <= 0) {

				users.put(uuid, intitalexp);

			} else {
				users.computeIfPresent(uuid, (num, val) -> getExp(player) - amount);

				users.computeIfAbsent(uuid, num -> intitalexp);
			}
		}

	}

	public Integer getneededXP(Player player) {
		int level = getLevel(player);

		if (getLevel(player) == maximumlevelCapacity - 1) {
			return 0;
		} else {
			int formula = levelcurve.get(level + 1) - getExp(player);

			return formula;
		}
	}

	public void loadExp(UUID key, int value) {
		users.put(key, value);
	}

	public void initializeLevels() {
		for (int i = starterlevel; i < maximumlevelCapacity - 1; i++) {

			int formula = i * difficulty;

			levelcurve.add(baseCurve * formula);
		}
	}

	public Integer getLevel(Player player) {
		int exp = getExp(player);

		int returnValue = starterlevel;

		for (int i = starterlevel; i < maximumlevelCapacity - 1; i++) {

			int formula = (maximumlevelCapacity - 1) * difficulty;

			if (getExp(player) >= baseCurve * formula) {

				setExp(player, baseCurve * formula);

				return maximumlevelCapacity - 1;
			}
			if (exp < levelcurve.get(i) && exp >= levelcurve.get(i - 1)) {
				returnValue = (i - 1);
				return returnValue;
			}

		}

		return returnValue;

	}

	public void setLevel(Player player, int level) {
		int needed = getneededXP(player);

		if (getLevel(player) == maximumlevelCapacity - 1) {
			player.sendMessage("You have reached max level!");
		} else {

			setExp(player, needed * level);

		}
	}
	
	public void addLevel(Player player, int level){
		
		setLevel(player, getLevel(player) + level);
		
	}
	
	public void subtractLevel(Player player, int level){
		
		setLevel(player, getLevel(player) - level);
	}

}
