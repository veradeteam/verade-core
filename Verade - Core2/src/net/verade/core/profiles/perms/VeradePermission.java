package net.verade.core.profiles.perms;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor @Getter
public class VeradePermission {

	private String permission;
	private boolean enabled;
	
	public VeradePermission(String perm) {
		if (perm.startsWith("-")) {
			permission=perm.substring(1);
			enabled=false;
		}else{
			permission=perm;
			enabled=true;
		}
	}
	
	public boolean equals(VeradePermission perm) {
		return perm.getPermission().equals(getPermission()) && perm.isEnabled() == isEnabled();
	}
	
}