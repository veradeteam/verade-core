package net.verade.core.profiles.perms.utils;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftHumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissibleBase;

public class PermissibleInjector implements Listener {
	
  	public Permissible inject(Player player, Permissible permissible) throws NoSuchFieldException, IllegalAccessException {
  		Field permField = getPermissibleField(player);
  		if (permField == null) {
  			return null;
  		}
  		Permissible oldPerm = (Permissible)permField.get(player);
  		if ((permissible instanceof PermissibleBase)) {
  			PermissibleBase newBase = (PermissibleBase)permissible;
  			PermissibleBase oldBase = (PermissibleBase)oldPerm;
  			copyValues(oldBase, newBase);
    	}
    	permField.set(player, permissible);
    	return oldPerm;
  	}
  
  	public Permissible getPermissible(Player player) throws NoSuchFieldException, IllegalAccessException {
	  return (Permissible)getPermissibleField(player).get(player);
  	}
  
    private Field getPermissibleField(Player player) throws NoSuchFieldException {
    	CraftHumanEntity human = (CraftHumanEntity) player;
    	Class<?> humanEntity = human.getClass().getSuperclass();
    	Field permField = humanEntity.getDeclaredField("perm");
    	permField.setAccessible(true);
    	return permField;
    }
  
    @SuppressWarnings("unchecked")
    private void copyValues(PermissibleBase old, PermissibleBase newPerm) throws NoSuchFieldException, IllegalAccessException {
    	Field attachmentField = PermissibleBase.class.getDeclaredField("attachments");
    	attachmentField.setAccessible(true);
    	List<Object> attachmentPerms = (List<Object>)attachmentField.get(newPerm);
    	attachmentPerms.clear();
    	attachmentPerms.addAll((List<Object>)attachmentField.get(old));
    	newPerm.recalculatePermissions();
    }

}

