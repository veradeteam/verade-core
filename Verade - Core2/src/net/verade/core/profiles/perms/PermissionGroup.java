package net.verade.core.profiles.perms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;

import lombok.Getter;

public class PermissionGroup {

	@Getter List<String> inherits = new ArrayList<>();
	@Getter List<VeradePermission> permissions = new ArrayList<>();
	@Getter HashMap<String, List<VeradePermission>> worldPermissions = new HashMap<>();
	HashMap<String, Object> options = new HashMap<>();
	@Getter int weight;
	
	@Getter List<VeradePermission> allGlobalPerms = new ArrayList<>();
	
	public PermissionGroup(ConfigurationSection data) {
		weight = data.getInt("weight");
		
		if (data.contains("inheritance")) {
			for (String inherit : data.getStringList("inheritance")) {
				if (PermissionManager.getManager().isGroup(inherit)) {
					inherits.add(inherit);
					
					allGlobalPerms.addAll(PermissionManager.getManager().getPermissionGroup(inherit).getAllGlobalPerms());
				}	
			}
		}
		
		if (data.contains("permissions")) {
			for (String perm : data.getStringList("permissions")) {
				boolean enabled = true;
				if (perm.startsWith("-")) {
					enabled=false;
					perm = perm.substring(1);
				}
				permissions.add(new VeradePermission(perm, enabled));
				allGlobalPerms.add(new VeradePermission(perm, enabled));
			}	
		}
		if (data.contains("worlds")) {
			for (String world : data.getConfigurationSection("worlds").getKeys(false)) {
				if (!worldPermissions.containsKey(world)) {
					worldPermissions.put(world, new ArrayList<VeradePermission>());
				}
				
				for (String perm : data.getStringList("worlds." + world)) {
					boolean enabled = true;
					if (perm.startsWith("-")) {
						enabled=false;
						perm = perm.substring(1);
					}
					worldPermissions.get(world).add(new VeradePermission(perm, enabled));
				}
			}	
		}
		if (data.contains("options")) {
			for (String option : data.getConfigurationSection("options").getKeys(false)) {
				options.put(option, data.get("options." + option));
			}	
		}
		
	}
	
	public List<VeradePermission> getGroupPermissions(String worldName) {
		if (worldPermissions.containsKey(worldName)) {
			return worldPermissions.get(worldName);
		}
		return new ArrayList<>();
	}
	
	HashMap<String, List<VeradePermission>> permCache = new HashMap<>();
	public List<VeradePermission> getAllPerms(String worldName) {
		if (permCache.containsKey(worldName))return permCache.get(worldName);
		
		ArrayList<VeradePermission> perms = new ArrayList<>();
		
		for (String group : inherits) {
			perms.addAll(PermissionManager.getManager().getPermissionGroup(group).getAllPerms(worldName));
		}
 
		perms.addAll(permissions);
		perms.addAll(getGroupPermissions(worldName));
		
		permCache.put(worldName, perms);
		
		return perms;
	}
	
	HashMap<String, Object> cachedAllOptions = null;
	
	public HashMap<String, Object> getGroupOptions() {
		return options;
	}
	
	public Object getGroupOption(String option) {
		return options.get(option);
	}
	
	public HashMap<String, Object> getAllOptions() {
		if (cachedAllOptions==null) {
			cachedAllOptions = new HashMap<String, Object>();
			for (String group : inherits) {
				cachedAllOptions.putAll(PermissionManager.getManager().getPermissionGroup(group).getAllOptions());
			}
			cachedAllOptions.putAll(options);
		}
		return cachedAllOptions;
	}
	
	public Object getOption(String option) {
		return getAllOptions().get(option);
	}
	
}

