package net.verade.core.profiles.perms;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

public class VeradePermissible extends PermissibleBase {

	private Permissible innerPerm;

	public VeradePermissible(Player player) {
		super(player);
	}

	public void setPreviousPermissible(Permissible previousPermissible) {
		this.innerPerm = previousPermissible;
	}

	public Permissible getPreviousPermissible() {
		return this.innerPerm;
	}

	public boolean hasPermission(Permission perm) {
		return hasPermission(perm.getName());
	}

	public boolean hasPermission(String check_name) {
		Set<PermissionAttachmentInfo> perms = getEffectivePermissions();

		boolean ret = false;
		for (PermissionAttachmentInfo perm : perms) {
			String vs_name = perm.getPermission();
			boolean isNegative = (perm.getValue() == false);
			boolean isApplicable = doPermCompare(check_name, vs_name);
			if (isApplicable) {
				if (isNegative) {
					ret = false;
				} else {
					ret = true;
				}
			}
		}
		return ret;
	}

	public boolean doPermCompare(String check_name, String vs_name) {
		if (check_name.equalsIgnoreCase(vs_name)) {
			return true;
		}
		Iterator<String> check_iter = Arrays.asList(check_name.split("\\.")).iterator();
		Iterator<String> vs_iter = Arrays.asList(vs_name.split("\\.")).iterator();
		while ((check_iter.hasNext()) && (vs_iter.hasNext())) {
			String check_part = (String) check_iter.next();
			String vs_part = (String) vs_iter.next();
			if ((!vs_iter.hasNext()) && (vs_part.equals("*"))) {
				return true;
			}
			if ((vs_part != "*") && (!vs_part.equalsIgnoreCase(check_part))) {
				return false;
			}
		}
		return false;
	}

	public PermissionAttachment addAttachment(Plugin plugin) {
		return this.innerPerm.addAttachment(plugin);
	}

	public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
		return this.innerPerm.addAttachment(plugin, ticks);
	}

	public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
		return this.innerPerm.addAttachment(plugin, name, value);
	}

	public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
		return this.innerPerm.addAttachment(plugin, name, value, ticks);
	}

	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		return this.innerPerm.getEffectivePermissions();
	}

	public boolean isPermissionSet(Permission perm) {
		return this.innerPerm.isPermissionSet(perm);
	}

	public boolean isPermissionSet(String name) {
		return this.innerPerm.isPermissionSet(name);
	}

	public void recalculatePermissions() {
		if (this.innerPerm != null) {
			this.innerPerm.recalculatePermissions();
		}
	}

	public void removeAttachment(PermissionAttachment attachment) {
		this.innerPerm.removeAttachment(attachment);
	}

}
