package net.verade.core.profiles.perms.cmds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;

import net.verade.core.Verade;
import net.verade.core.cmds.VeradeCommand;
import net.verade.core.cmds.VeradeSubCommand;

public class PermCommand extends VeradeCommand {

	List<VeradeSubCommand> subCommands = new ArrayList<>();
	
	public PermCommand(Verade verade) {
		super("perms", "Base command for permissions.", "verade.perms", "permissions", "pex");
		subCommands.add(new PermsUserCommand(verade));
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length==0) {
			showHelp(sender);
		}else{
			for (VeradeSubCommand sub : subCommands) {
				if (isSub(args[0], sub)) {
					sub.execute(sender, Arrays.copyOfRange(args, 1, args.length));
					return;
				}
			}
			showHelp(sender);
		}
	}

	private void showHelp(CommandSender sender) {
		sender.sendMessage(colorize("&8- &a/perms user <user> &7View info on a user."));
		sender.sendMessage(colorize("&8- &a/perms user <user> add <permission> &7Add the specified permission to a user."));
		sender.sendMessage(colorize("&8- &a/perms user <user> remove <permission> &7Remove the specified permission from a user."));
		sender.sendMessage(colorize("&8- &a/perms user <user> group set <group> &7Set the group of a user to the specified group."));
		sender.sendMessage(colorize("&8- &a/perms user <user> group add <group> &7Add a user to the specified group."));
		sender.sendMessage(colorize("&8- &a/perms user <user> group remove <group> &7Remove a user from the specified group."));
		sender.sendMessage(colorize("&8- &a/perms group <group> &7View info on a group."));
		sender.sendMessage(colorize("&8- &a/perms group <group> add <perm> [world] &7Add a perm to a group."));
		sender.sendMessage(colorize("&8- &a/perms reload &7Reload Permissions."));
	}

	private boolean isSub(String label, VeradeSubCommand sub) {
		if (label.equalsIgnoreCase(sub.getLabel()))return true;
		for (String alias : sub.getAliases()) {
			if (alias.equalsIgnoreCase(label))return true;
		}
		return false;
	}
	
	/*
	 * View User Info
	 * View Group Info
	 * Set User Group
	 * Set User Staff Group
	 * Remove User Extra Group
	 * 
	 */

}
