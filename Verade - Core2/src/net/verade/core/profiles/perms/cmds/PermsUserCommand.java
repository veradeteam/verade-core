package net.verade.core.profiles.perms.cmds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.verade.core.Verade;
import net.verade.core.cmds.VeradeSubCommand;
import net.verade.core.profiles.Profile;
import net.verade.core.profiles.ProfileManager;
import net.verade.core.profiles.perms.VeradePermission;
import net.verade.core.profiles.perms.PermissionManager;


public class PermsUserCommand extends VeradeSubCommand {

	List<VeradeSubCommand> subCommands = new ArrayList<>();
	Verade verade;
	
	protected PermsUserCommand(Verade verade) {
		super("user", "All commands relating to users.", "player");
		this.verade=verade;
	}

	@Override
	public void execute(final CommandSender sender, String[] args) {
		if (args.length==0) {
			sender.sendMessage(colorize("&cUse /perms user <user>"));
		}else{
			final String user = args[0];
			if (args.length==1) {
				if (sender.hasPermission("verade.perms.user.info")) {
					Player target = getPlayer(user);
					if (target != null) {
						Profile profile = ProfileManager.getManager().getProfile(target);
						showUserInfo(sender, profile);
					}else{
						new BukkitRunnable() {
							@Override
							public void run() {
								Profile profile = ProfileManager.getManager().getProfileFromMongo(user);
								if (profile==null) {
									sender.sendMessage(colorize("&cNo user found for " + user + "."));
								}else{
									profile.loadPerms();
									showUserInfo(sender, profile);
								}
							}
						}.runTaskAsynchronously(verade);
					}
				}else{
					noPermission(sender, args);
				}
				return;
			}else{
				String cmd = args[1];
				if (cmd.equalsIgnoreCase("group")) {
					if (args.length > 3) {
						String group = args[3];
						if (sender.hasPermission("verade.perms.user.groups." + group)) {
							if (args[2].equalsIgnoreCase("set")) {		
								setGroup(sender, user, group);
							}else if (args[2].equalsIgnoreCase("add")) {
								addGroup(sender, user, group);
							}else if (args[2].equalsIgnoreCase("remove")) {
								removeGroup(sender, user, group);
							}else{
								sender.sendMessage(colorize("&cCommand not found. Use /perms for help."));
							}
						}else{
							sender.sendMessage(colorize("&cYou don't have permission for the group " + group + "."));
						}
					}else{
						sender.sendMessage(colorize("&cCommand not found. Use /perms for help."));
					}
				}else if (cmd.equalsIgnoreCase("add")) {
					if (args.length > 2) {
						if (sender.hasPermission("verade.perms.user.add." + args[2])) {
							addPerm(sender, user, args[2]);	
						}else{
							sender.sendMessage(colorize("&cYou don't have permission to add " + args[2] + "."));
						}
					}else{
						sender.sendMessage(colorize("&cCommand not found. Use /perms for help."));
					}
				}else if (cmd.equalsIgnoreCase("remove")) {
					if (args.length > 2) {
						if (sender.hasPermission("verade.perms.user.remove." + args[2])) {
							removePerm(sender, user, args[2]);	
						}else{
							sender.sendMessage(colorize("&cYou don't have permission to remove " + args[2] + "."));
						}
					}else{
						sender.sendMessage(colorize("&cCommand not found. Use /perms for help."));
					}
				}
			}
		}
	}
	
	public void showUserInfo(CommandSender sender, Profile profile) {
		sender.sendMessage(colorize("&7Showing perm info for &a" + profile.getUsername() + "&7:"));
		sender.sendMessage(colorize("&7Groups:"));
		for (String group : profile.getGroups()) {
			sender.sendMessage(colorize("&8- &a" + group + (profile.isActiveGroup(group) ? " &8(Active)" : "")  ));
		}
		sender.sendMessage(colorize("&7Permissions:"));
		for (VeradePermission perm : profile.getPermissions()) {
			String permName = perm.getPermission();
			if (!perm.isEnabled())permName="-"+permName;
			sender.sendMessage(colorize("&8- &a") + permName);
		}
	}
	
	public void setGroup(final CommandSender sender, final String user, final String group) {
		Player target = getPlayer(user);
		if (target != null) {
			Profile targetProfile = ProfileManager.getManager().getProfile(target);
			ArrayList<String> setGroups = new ArrayList<>();
			setGroups.add(group);
			targetProfile.setGroups(setGroups);
			if (PermissionManager.getManager().isGroup(group)) {
				TreeMap<Integer, String> activeGroup = new TreeMap<>();
				activeGroup.put(PermissionManager.getManager().getPermissionGroup(group).getWeight(), group);
				targetProfile.setActiveGroups(activeGroup);
			}else{
				TreeMap<Integer, String> activeGroup = new TreeMap<>();
				activeGroup.put(PermissionManager.getManager().getPermissionGroup("Default").getWeight(), "Default");
				targetProfile.setActiveGroups(activeGroup);
			}
			PermissionManager.getManager().givePermissons(target);
			targetProfile.set("perms.groups", Arrays.asList(group));
			sender.sendMessage(colorize("&8[&a&l*&8] &7User group set. Note: Group names are case sensitive."));
		}else{
			sender.sendMessage(colorize("&8[&a&l*&8] &7Seaching Users"));
			new BukkitRunnable() {
				@Override
				public void run() {
					Profile profile = ProfileManager.getManager().getProfileFromMongo(user);
					if (profile == null) {
						sender.sendMessage(colorize("&8[&a&l*&8] &7Could not find the user &a" + user + "&7."));
						return;
					}
					profile.set("perms.groups", Arrays.asList(group));
					sender.sendMessage(colorize("&8[&a&l*&8] &7User group set. Note: Group names are case sensitive."));
				}
			}.runTaskAsynchronously(verade);
		}
	}
	
	public void addGroup(final CommandSender sender, final String user, final String group) {
		Player target = getPlayer(user);
		if (target != null) {
			Profile targetProfile = ProfileManager.getManager().getProfile(target);
			if (targetProfile.getGroups().contains(group)) {
				sender.sendMessage(colorize("&8[&a&l*&8] &7User already in group."));
				return;
			}
			targetProfile.addGroup(group);	
			PermissionManager.getManager().givePermissons(target);
			sender.sendMessage(colorize("&8[&a&l*&8] &7User group added. Note: Group names are case sensitive."));
		}else{
			sender.sendMessage(colorize("&8[&a&l*&8] &7Seaching Users"));
			new BukkitRunnable() {
				@Override
				public void run() {
					Profile profile = ProfileManager.getManager().getProfileFromMongo(user);
					if (profile == null) {
						sender.sendMessage(colorize("&8[&a&l*&8] &7Could not find the user &a" + user + "&7."));
						return;
					}
					profile.loadPerms();				
					profile.addGroup(group);		
					sender.sendMessage(colorize("&8[&a&l*&8] &7User group added. Note: Group names are case sensitive."));
				}
			}.runTaskAsynchronously(verade);
		}
	}
	
	public void removeGroup(final CommandSender sender, final String user, final String group) {
		Player target = getPlayer(user);
		if (target != null) {
			Profile targetProfile = ProfileManager.getManager().getProfile(target);
			if (!targetProfile.getGroups().contains(group)) {
				sender.sendMessage(colorize("&8[&a&l*&8] &7User not in group."));
				return;
			}
			targetProfile.removeGroup(group);
			PermissionManager.getManager().givePermissons(target);
			sender.sendMessage(colorize("&8[&a&l*&8] &7User group removed. Note: Group names are case sensitive."));
		}else{
			sender.sendMessage(colorize("&8[&a&l*&8] &7Seaching Users"));
			new BukkitRunnable() {
				@Override
				public void run() {
					Profile profile = ProfileManager.getManager().getProfileFromMongo(user);
					if (profile == null) {
						sender.sendMessage(colorize("&8[&a&l*&8] &7Could not find the user &a" + user + "&7."));
						return;
					}
					profile.loadPerms();				
					profile.removeGroup(group);			
					sender.sendMessage(colorize("&8[&a&l*&8] &7User group removed. Note: Group names are case sensitive."));
				}
			}.runTaskAsynchronously(verade);
		}
	}
	
	public void addPerm(final CommandSender sender, final String user, final String perm) {
		Player target = getPlayer(user);
		if (target != null) {
			Profile profile = ProfileManager.getManager().getProfile(target);
			profile.addPermission(perm);
			sender.sendMessage(colorize("&8[&a&l*&8] &7Permission &a" + perm + "&7 added."));
			
			PermissionManager.getManager().givePermissons(target);
		}else{
			sender.sendMessage(colorize("&8[&a&l*&8] &7Seaching Users"));
			new BukkitRunnable() {
				@Override
				public void run() {
					Profile profile = ProfileManager.getManager().getProfileFromMongo(user);
					if (profile == null) {
						sender.sendMessage(colorize("&8[&a&l*&8] &7Could not find the user &a" + user + "&7."));
						return;
					}
					profile.loadPerms();
					profile.addPermission(perm);
					sender.sendMessage(colorize("&8[&a&l*&8] &7Permission &a" + perm + "&7 added."));
				}
			}.runTaskAsynchronously(verade);
		}
	}
	
	public void removePerm(final CommandSender sender, final String user, final String perm) {
		Player target = getPlayer(user);
		if (target != null) {
			Profile profile = ProfileManager.getManager().getProfile(target);
			profile.removePermission(perm);
			sender.sendMessage(colorize("&8[&a&l*&8] &7Permission &a" + perm + "&7 removed."));
			
			PermissionManager.getManager().givePermissons(target);
		}else{
			sender.sendMessage(colorize("&8[&a&l*&8] &7Seaching Users"));
			new BukkitRunnable() {
				@Override
				public void run() {
					Profile profile = ProfileManager.getManager().getProfileFromMongo(user);
					if (profile == null) {
						sender.sendMessage(colorize("&8[&a&l*&8] &7Could not find the user &a" + user + "&7."));
						return;
					}
					profile.loadPerms();
					profile.removePermission(perm);
					sender.sendMessage(colorize("&8[&a&l*&8] &7Permission &a" + perm + "&7 removed."));
				}
			}.runTaskAsynchronously(verade);
		}
	}

}
