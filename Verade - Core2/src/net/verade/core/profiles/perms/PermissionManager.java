package net.verade.core.profiles.perms;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachment;

import lombok.Getter;
import net.verade.core.Verade;
import net.verade.core.profiles.Profile;
import net.verade.core.profiles.ProfileManager;
import net.verade.core.profiles.perms.cmds.PermCommand;
import net.verade.core.profiles.perms.utils.PermissibleInjector;
import net.verade.core.util.CoreSettings;

public class PermissionManager implements Listener {

	static PermissionManager pm = new PermissionManager();
	private PermissionManager() {}
	
	PermissibleInjector injector = new PermissibleInjector();
	Verade verade;
	
	HashMap<String, PermissionGroup> permissionGroups = new HashMap<>();
	HashMap<UUID, PermissionAttachment> permissionAttachments = new HashMap<>();
	
	@Getter FileConfiguration permissionsConfiguration; 
	File permissionsFile;
	
	public static PermissionManager getManager() {
		return pm;
	}
	
	public void setup(Verade verade) {
		this.verade=verade;
		loadFile();
		
		if (!getPermissionsConfiguration().isConfigurationSection("groups"))return;
		for (String group : getPermissionsConfiguration().getConfigurationSection("groups").getKeys(false)) {
			permissionGroups.put(group, new PermissionGroup(getPermissionsConfiguration().getConfigurationSection("groups." + group)));
		}
		
		Bukkit.getPluginManager().registerEvents(this, verade);
		
		new PermCommand(verade).registerMe();
	}
	
	public PermissionGroup getPermissionGroup(String group) {
		return permissionGroups.get(group);
	}
	
	public boolean isGroup(String group) {
		return permissionGroups.containsKey(group);
	}
	
	public void loadUser(Player p, Profile profile) {
		VeradePermissible permissible = new VeradePermissible(p);
		injectPermissible(p, permissible);
		PermissionAttachment attachment = permissible.addAttachment(verade);
		permissionAttachments.put(p.getUniqueId(), attachment);
	}
	
	public void unloadUser(Player p) {
		removePerms(p);
		try {
			uninjectPermissible(p, (VeradePermissible) injector.getPermissible(p));
		} catch (Exception e) {
			e.printStackTrace();
		}
		permissionAttachments.remove(p.getUniqueId());
	}
	
	public PermissionAttachment getAttachment(Player p) {
		return permissionAttachments.get(p.getUniqueId());
	}
	
	public void removePerms(Player p) {
		permissionAttachments.get(p.getUniqueId()).getPermissions().clear();
		p.recalculatePermissions();
	}
	
	public void giveAllGlobalPermissions(Player p, Profile profile) {
		PermissionAttachment attachment = permissionAttachments.get(p.getUniqueId());
		if (attachment==null) {
			p.sendMessage(ChatColor.RED + "An error occured loading your data. Please try logging in again.");
			p.kickPlayer("");
			return;
		}
		for (String perm : attachment.getPermissions().keySet()) {
			attachment.unsetPermission(perm);
		}
		
		HashMap<String, Object> options = new HashMap<>();
		
		for (VeradePermission permission : profile.getPermissions()) {
			attachment.setPermission(permission.getPermission(), permission.isEnabled());
		}
		
		for (String groupName : profile.getActiveGroups().values()) {
			PermissionGroup group = permissionGroups.get(groupName);

			for (VeradePermission permission : group.getAllGlobalPerms()) {
				attachment.setPermission(permission.getPermission(), permission.isEnabled());
			}

			options.putAll(group.getAllOptions());
		}
		
		profile.setOptions(options);

		if (CoreSettings.isControlTabList() && options.containsKey("tablistprefix")) {
			p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', (String) options.get("tablistprefix")) + p.getName());
		}
		
		if (CoreSettings.isControlDisplayName()) {
			String name = p.getName();
			if (CoreSettings.isAllowColoredName()) {
				if (p.hasPermission("verade.namecolor") && profile.getNameColor() != null) {
					name=profile.getNameColor()+name;
				}
			}
			if (options.containsKey("prefix")) {
				String prefix = ChatColor.translateAlternateColorCodes('&', (String) profile.getOption("prefix"));
				p.setDisplayName(prefix + name);
			}
		}
		
		p.recalculatePermissions();
	}
	
	public void givePermissons(Player p) {
		givePermissions(p, p.getWorld().getName());
	}
	
	public void givePermissons(Player p, Profile profile) {
		givePermissions(p, profile, p.getWorld().getName());
	}
	
	public void givePermissions(Player p, String worldName) {
		givePermissions(p, ProfileManager.getManager().getProfile(p), worldName);
	}
	
	public void givePermissions(Player p, Profile profile, String worldName) {
		PermissionAttachment attachment = permissionAttachments.get(p.getUniqueId());
		
		for (String perm : attachment.getPermissions().keySet()) {
			attachment.unsetPermission(perm);
		}
		
		HashMap<String, Object> options = new HashMap<>();
		
		for (VeradePermission permission : profile.getPermissions()) {
			attachment.setPermission(permission.getPermission(), permission.isEnabled());
		}
		
		for (String groupName : profile.getActiveGroups().values()) {
			PermissionGroup group = permissionGroups.get(groupName);

			for (VeradePermission permission : group.getAllPerms(worldName)) {
				attachment.setPermission(permission.getPermission(), permission.isEnabled());
			}

			options.putAll(group.getAllOptions());
		}
		
		profile.setOptions(options);

		giveOptions(p, profile);


		p.recalculatePermissions();
	}
	
	public void giveOptions(Player p, Profile profile) {
		HashMap<String, Object> options = profile.getOptions();
		if (CoreSettings.isControlTabList() && options.containsKey("tablistprefix")) {
			p.setPlayerListName(ChatColor.translateAlternateColorCodes('&', (String) options.get("tablistprefix")) + p.getName());
		}
		
		if (CoreSettings.isControlDisplayName()) {
			String name = p.getName();
			if (CoreSettings.isAllowColoredName()) {
				if (p.hasPermission("verade.namecolor") && profile.getNameColor() != null) {
					name=profile.getNameColor()+name;
				}
			}
			if (options.containsKey("prefix")) {
				String prefix = ChatColor.translateAlternateColorCodes('&', (String) profile.getOption("prefix"));
				p.setDisplayName(prefix + name);
			}
		}
	}
	
	public void loadFile() {
		 if (!verade.getDataFolder().exists()) {
	        	verade.getDataFolder().mkdir();
		 }
		 permissionsFile = new File(verade.getDataFolder(), "permissions.yml");
		 if (!permissionsFile.exists()) {
			 try {
				 verade.saveResource("permissions.yml", true);
				 permissionsFile.createNewFile();
			 } catch (IOException e) {
				 e.printStackTrace();
			 }
		 }
		 permissionsConfiguration = YamlConfiguration.loadConfiguration(permissionsFile);
	}
	
	public void savePermissionFile() {
		try {
			permissionsConfiguration.save(permissionsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void injectPermissible(Player p, VeradePermissible permissible) {
		try {
			Permissible oldPerm = injector.inject(p, permissible);
			permissible.setPreviousPermissible(oldPerm);  
			permissible.recalculatePermissions();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void uninjectPermissible(Player player, VeradePermissible permissible) {
		  try {
			injector.inject(player, permissible.getPreviousPermissible());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//======================================={Util}=======================================
	
	public void addPermission(Player p, String perm) {
		ProfileManager.getManager().getProfile(p).addPermission(perm);
		givePermissons(p);
	}
	
	public void removePermission(Player p, String perm) {
		ProfileManager.getManager().getProfile(p).removePermission(perm);
		givePermissons(p);
	}
	
	@EventHandler
	public void onChangeWorld(PlayerChangedWorldEvent e) {
		givePermissions(e.getPlayer(), e.getPlayer().getWorld().getName());
	}
	
}

