package net.verade.core.profiles.events;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import lombok.Getter;
import net.verade.core.profiles.Profile;

public class PlayerProfileLoadEvent extends PlayerEvent {

	@Getter Profile profile;
	@Getter HashMap<String, Object> unsets;
	@Getter HashMap<String, Object> sets;
	
	public PlayerProfileLoadEvent(Player player, Profile profile, HashMap<String, Object> unsets, HashMap<String, Object> sets) {
		super(player);
		this.profile=profile;
		this.unsets=unsets;
		this.sets=sets;
	}
	
	public PlayerProfileLoadEvent(Player player, Profile profile) {
		this(player, profile, new HashMap<String, Object>(), new HashMap<String, Object>());
	}

	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers()
	{
		return handlers;
	}
	  
	public static HandlerList getHandlerList()
	{
		return handlers;
	}
	
}
