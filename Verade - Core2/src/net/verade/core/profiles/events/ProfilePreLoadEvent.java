package net.verade.core.profiles.events;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;

public class ProfilePreLoadEvent extends Event {

	@Getter UUID uuid;
	@Getter String username;
	@Getter HashMap<String, Object> unsets;
	@Getter HashMap<String, Object> sets;
	
	public ProfilePreLoadEvent(UUID uuid, String username, HashMap<String, Object> unsets, HashMap<String, Object> sets) {
		this.uuid=uuid;
		this.username=username;
		this.unsets=unsets;
		this.sets=sets;
	}
	
	public ProfilePreLoadEvent(UUID uuid, String username) {
		this.uuid=uuid;
		this.username=username;
		this.unsets=new HashMap<String, Object>();
		this.sets=new HashMap<String, Object>();
	}

	private static final HandlerList handlers = new HandlerList();
	
	public HandlerList getHandlers()
	{
		return handlers;
	}
	  
	public static HandlerList getHandlerList()
	{
		return handlers;
	}
	
}
