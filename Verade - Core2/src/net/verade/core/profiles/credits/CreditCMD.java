package net.verade.core.profiles.credits;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import net.verade.core.Verade;
import net.verade.core.cmds.VeradeCommand;
import net.verade.core.profiles.Profile;
import net.verade.core.profiles.ProfileManager;
import net.verade.core.profiles.util.ModifyType;
import net.verade.core.profiles.util.ProfileModifier;

public class CreditCMD extends VeradeCommand {

	Verade verade;
	
	public CreditCMD(Verade verade) {
		super("credits", "Used for credit related operations.", "verade.credits", "credit");
		this.verade=verade;
	}

	@Override
	public void execute(final CommandSender sender, final String[] args) {
		if (args.length > 1) {
			if (args[0].equalsIgnoreCase("amount") || args[0].equalsIgnoreCase("amnt")) {
				if (sender.hasPermission("verade.credits.amount")) {
					Player target = getPlayer(args[1]);
					if (target != null) {
						sender.sendMessage(colorize("&8[&b&l*&8] &b" + target.getName() + " &7has &b" + ProfileManager.getManager().getProfile(target).getCredits() + " credits&7."));
					}else{
						sender.sendMessage(colorize("&8[&b&l*&8] &7Searching Users..."));
						new BukkitRunnable() {
							@Override
							public void run() {
								Profile profile = ProfileManager.getManager().getProfileFromMongo(args[1]);
								if (profile != null) {
									profile.loadCredits();
									sender.sendMessage(colorize("&8[&b&l*&8] &b" + profile.getUsername() + " &7has &b" + profile.getCredits() + " credits&7."));
								}else{
									sender.sendMessage(colorize("&8[&b&l*&8] &7Could not find user with key &b" + args[1] + "&7."));
								}
							}
						}.runTaskAsynchronously(verade);
					}
				}
			}else if (args[0].equalsIgnoreCase("modify")) {
				if (sender.hasPermission("verade.credits.modify")) {
					if (args.length==2) {
						sender.sendMessage(colorize("&8- &b/credits modify <user> <amount>"));
						return;
					}
					
					final int amount;
					try {
						amount = Integer.valueOf(args[2]);
					}catch(NumberFormatException nfe) {  
						sender.sendMessage(colorize("&8- &b/credits modify <user> <amount>"));
						return;
					}  
					Player target = getPlayer(args[1]);
					if (target != null) {
						sender.sendMessage(colorize("&8[&b&l*&8] &7Modified &b" + target.getName() + "'s credits &7by &b" + amount + "&7."));
						new ProfileModifier(ProfileManager.getManager().getProfile(target)).modify(ModifyType.CREDITS, amount).save();
					}else{
						sender.sendMessage(colorize("&8[&b&l*&8] &7Searching Users..."));
						new BukkitRunnable() {
							@Override
							public void run() {
								Profile profile = ProfileManager.getManager().getProfileFromMongo(args[1]);
								if (profile != null) {
									profile.loadCredits();
									sender.sendMessage(colorize("&8[&b&l*&8] &7Modified &b" + profile.getUsername() + "'s credits &7by &b" + amount + "&7."));
									new ProfileModifier(profile).modify(ModifyType.CREDITS, amount).save();
								}else{
									sender.sendMessage(colorize("&8[&b&l*&8] &7Could not find user with key &b" + args[1] + "&7."));
								}
							}
						}.runTaskAsynchronously(verade);
					}
				}
			}else{
				sender.sendMessage(colorize("&8- &b/credits modify <user> <amount>"));
				sender.sendMessage(colorize("&8- &b/credits amount <user>"));
			}
		}else{
			sender.sendMessage(colorize("&8- &b/credits modify <user> <amount>"));
			sender.sendMessage(colorize("&8- &b/credits amount <user>"));
		}
	}
	
}
