package net.verade.core.profiles.credits;

import net.verade.core.Verade;

public class CreditManager {

	static CreditManager manager = new CreditManager();
	private CreditManager() {};
	
	public static CreditManager getManager() {
		return manager;
	}
	
	Verade verade;

	public void setup(Verade verade) {
		this.verade=verade;
		new CreditCMD(verade).registerMe();
	}
	
}