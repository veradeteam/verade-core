package net.verade.core.profiles.util;

import org.bukkit.ChatColor;

import lombok.Getter;

public enum ModifyType {

	CREDITS("credits"),  NAME_COLOR("color")
	
	;
	
	@Getter String path;
	
	private ModifyType(String path) {
		this.path=path;
	}
	
	public void modifyForProfile(ProfileModifier profileModifier, Object modified) {
		switch (this) {
		case CREDITS:
			profileModifier.getProfile().modifyCredits((int) modified);
			profileModifier.getSave().inc(getPath(), (int) modified);
			break;
		case NAME_COLOR:
			if (modified == null) {
				profileModifier.getProfile().setNameColor(null);
				profileModifier.getSave().unset(getPath(), 1);
			}else{
				ChatColor color = (ChatColor) modified;
				profileModifier.getProfile().setNameColor(color);
				profileModifier.getSave().set(getPath(), color.name());	
			}
			break;
		default:
			break;
		}
	}
	
}
