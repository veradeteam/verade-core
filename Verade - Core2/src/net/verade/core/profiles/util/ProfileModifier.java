package net.verade.core.profiles.util;

import lombok.Getter;
import net.verade.core.profiles.Profile;

@Getter
public class ProfileModifier {

	Profile profile;
	ProfileSave save;
	
	public ProfileModifier(Profile profile) {
		this.profile=profile;
		save = new ProfileSave(profile);
	}
	
	public ProfileModifier modify(ModifyType type, Object modify) {
		type.modifyForProfile(this, modify);
		return this;
	}
	
	public void save() {
		save.save();
	}
	
}
