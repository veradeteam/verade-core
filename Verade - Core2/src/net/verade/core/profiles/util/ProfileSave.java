package net.verade.core.profiles.util;

import org.bson.Document;
import org.bukkit.scheduler.BukkitRunnable;

import com.mongodb.BasicDBObject;

import net.verade.core.MongoManager;
import net.verade.core.profiles.Profile;
import net.verade.core.util.mongo.Save;

public class ProfileSave extends Save {

	Profile profile;
	
	public ProfileSave(Profile profile) {
		this.profile=profile;
		this.cause=new BasicDBObject("uuid", profile.getUUID());
	}
	
	public void save() {
		new BukkitRunnable() {
			@Override
			public void run() {
				Document build = build();
				if (!build.isEmpty()) {
					MongoManager.getManager().getDatabase().getCollection("players").updateOne(cause, build);	
				}
			}
		}.runTaskAsynchronously(profile.getVerade());
	}
	
}
