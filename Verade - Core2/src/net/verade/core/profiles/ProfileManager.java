package net.verade.core.profiles;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;

import lombok.Getter;
import net.verade.core.MongoManager;
import net.verade.core.Verade;
import net.verade.core.profiles.events.PlayerProfileLoadEvent;
import net.verade.core.profiles.events.PlayerProfileUnLoadEvent;
import net.verade.core.profiles.events.ProfilePreLoadEvent;
import net.verade.core.profiles.levels.LevelManager;
import net.verade.core.profiles.perms.PermissionManager;

public class ProfileManager implements Listener {

	PluginManager pluginManager = Bukkit.getPluginManager();
	static ProfileManager pm = new ProfileManager();
	static Verade verade;
	@Getter
	HashMap<UUID, Profile> profiles = new HashMap<>();

	boolean allowFullJoin = true;

	private ProfileManager() {}

	public static ProfileManager getManager() {
		return pm;
	}

	public void setup(Verade verade) {
		ProfileManager.verade = verade;
		Bukkit.getPluginManager().registerEvents(this, verade);
	}

	public Profile getProfile(Player p) {
		return profiles.get(p.getUniqueId());
	}
	
	public Profile getProfileFromMongo(String text){
		if(text.length()==36) return getProfileFromMongoUUID(text);
		return getProfileFromMongoUsername(text);
	}
	
	public Profile getProfileFromMongoUUID(String uuid) {
		MongoCursor<Document> results = MongoManager.getManager().getDatabase().getCollection("players").find(new Document("uuid", uuid)).iterator();
		Profile profile = null;
		Document info;
		if (results.hasNext()) {
			info = results.next();
			profile = new Profile(verade, info.getString("uuid"), info);
		}
		results.close();
		return profile;
	}
	
	public Profile getProfileFromMongoUsername(String name) {
		MongoCursor<Document> results = MongoManager.getManager().getDatabase().getCollection("players").find(new Document("username", Pattern.compile(name, Pattern.CASE_INSENSITIVE))).iterator();
		Profile profile = null;
		Document info = null;
		
		while (results.hasNext()) {
			info=results.next();
			if (info.getString("username").equalsIgnoreCase(name)) {
				break;
			}
		}
		results.close();
		
		if (info==null)return null;
		
		profile = new Profile(verade, info.getString("uuid"), info);
		return profile;
	}
	
	public Profile loadProfileFromMongo(String uuid, String name, InetAddress address, HashMap<String, Object> sets, HashMap<String, Object> unsets) {
		Document info = null;
		
		if (sets.isEmpty()) {
			if (unsets.isEmpty()) {
				MongoCursor<Document> iterator = MongoManager.getManager().getDatabase().getCollection("players").find(new Document("uuid", uuid)).iterator();
				if (iterator.hasNext()) {
					info = iterator.next();
				}
				iterator.close();
			}else{
				info = MongoManager.getManager().getDatabase().getCollection("players").findOneAndUpdate(new BasicDBObject("uuid", uuid), new BasicDBObject("$unset", new BasicDBObject(unsets)));
			}
		}else{
			if (unsets.isEmpty()) {
				info = MongoManager.getManager().getDatabase().getCollection("players").findOneAndUpdate(new BasicDBObject("uuid", uuid), new BasicDBObject("$set", new BasicDBObject(sets)));
			}else{
				info = MongoManager.getManager().getDatabase().getCollection("players").findOneAndUpdate(new BasicDBObject("uuid", uuid), new BasicDBObject("$unset", new BasicDBObject(unsets)).append("$set", new BasicDBObject(sets)));
			}
		}
		
		Profile profile = null;
		
		if (info==null) {
			Bukkit.getLogger().info("[Verade-Core] New User Loaded (Didn't Update from Bungee)");
			info = getDefaultDocument(uuid, name,address.getHostAddress());
			MongoManager.getManager().getDatabase().getCollection("players").insertOne(info);
			profile = new Profile(verade, uuid, info);
			profile.setIP(address.getHostAddress());
			profile.save(sets, unsets);
		}
		profile = new Profile(verade, info.getString("uuid"), info);
		return profile;
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPreLogin(AsyncPlayerPreLoginEvent e) {
		ProfilePreLoadEvent event = new ProfilePreLoadEvent(e.getUniqueId(), e.getName());
		pluginManager.callEvent(event);
		Profile profile = loadProfileFromMongo(e.getUniqueId().toString(), e.getName(), e.getAddress(), event.getSets(), event.getUnsets());
		profile.setIP(e.getAddress().getHostAddress());
		profiles.put(e.getUniqueId(), profile);
	}

	@EventHandler(priority=EventPriority.LOWEST)
	public void onLogin(PlayerLoginEvent e) {
		Player p = e.getPlayer();
		Profile profile = profiles.get(p.getUniqueId());
		if (profile == null) {
			e.setResult(Result.KICK_OTHER);
			e.setKickMessage(ChatColor.RED + "An error occured.");
			return;
		}
		profile.load();
		PermissionManager.getManager().loadUser(p, profile);
		PermissionManager.getManager().giveAllGlobalPermissions(p, profile);
		if (e.getResult().equals(Result.KICK_FULL) && p.hasPermission("verade.reservedslot")) {
			e.allow();
		}
	}
	
    public String colorize(String text) {
    	return ChatColor.translateAlternateColorCodes('&', text);
    }
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		Profile profile = profiles.get(p.getUniqueId());
		PermissionManager.getManager().givePermissons(p);
		PlayerProfileLoadEvent event = new PlayerProfileLoadEvent(p, profile);
		Bukkit.getPluginManager().callEvent(event);
		profile.save(event.getUnsets(), event.getSets());
		profile.setLoaded(true);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onPostLogin(PlayerLoginEvent e) {
		if (!e.getResult().equals(PlayerLoginEvent.Result.ALLOWED)) {
			profiles.remove(e.getPlayer());
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent e) {
		quitPlayer(e.getPlayer());
	}
	
	public void quitPlayer(Player p) {
		PermissionManager.getManager().unloadUser(p);
		
		Profile profile = getProfile(p);
		if (profile==null||!profile.isLoaded())return;
		
		PlayerProfileUnLoadEvent event = new PlayerProfileUnLoadEvent(p, profile);
		Bukkit.getPluginManager().callEvent(event);
		profile.save(event.getUnsets(), event.getSets());
		profile.setLoaded(false);
	}
	
	
	private Document getDefaultDocument(String uuid, String name, String ip) {
		Document doc = new Document("uuid", uuid);
		doc.append("username", name);
		doc.append("ip", ip);
		doc.append("join", System.currentTimeMillis());
		doc.append("credits", 0);
		doc.append("level", LevelManager.getHandler().getLevel(Bukkit.getPlayer(name)));
		doc.append("exp", LevelManager.getHandler().getExp(Bukkit.getPlayer(name)));
		return doc;
	}
	
}
